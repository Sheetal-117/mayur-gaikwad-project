﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MayurProject.Data;
using MayurProject.Models;

namespace MayurProject.Controllers
{
    public class ApplicationsController : Controller
    {
        private readonly MayurProjectContext _context;

        public ApplicationsController(MayurProjectContext context)
        {
            _context = context;
        }

        // GET: Applications
        public async Task<IActionResult> Index()
        {
            var mayurProjectContext = _context.Application.Include(a => a.ApplicationAssetId).Include(a => a.ApplicationWebsiteId);
            return View(await mayurProjectContext.ToListAsync());
        }

        // GET: Applications/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var application = await _context.Application
                .Include(a => a.ApplicationAssetId)
                .Include(a => a.ApplicationWebsiteId)
                .FirstOrDefaultAsync(m => m.ApplicationId == id);
            if (application == null)
            {
                return NotFound();
            }

            return View(application);
        }

        // GET: Applications/Create
        public IActionResult Create()
        {
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId");
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId");
            return View();
        }

        // POST: Applications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AssetId,WebsiteId,ApplicationId,ApplicationName,ApplicationEapmId,ApplicationFtpPath")] Application application)
        {
            if (ModelState.IsValid)
            {
                _context.Add(application);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId", application.AssetId);
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId", application.WebsiteId);
            return View(application);
        }

        // GET: Applications/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var application = await _context.Application.FindAsync(id);
            if (application == null)
            {
                return NotFound();
            }
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId", application.AssetId);
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId", application.WebsiteId);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("AssetId,WebsiteId,ApplicationId,ApplicationName,ApplicationEapmId,ApplicationFtpPath")] Application application)
        {
            if (id != application.ApplicationId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(application);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationExists(application.ApplicationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId", application.AssetId);
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId", application.WebsiteId);
            return View(application);
        }

        // GET: Applications/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var application = await _context.Application
                .Include(a => a.ApplicationAssetId)
                .Include(a => a.ApplicationWebsiteId)
                .FirstOrDefaultAsync(m => m.ApplicationId == id);
            if (application == null)
            {
                return NotFound();
            }

            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var application = await _context.Application.FindAsync(id);
            _context.Application.Remove(application);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationExists(string id)
        {
            return _context.Application.Any(e => e.ApplicationId == id);
        }
    }
}
