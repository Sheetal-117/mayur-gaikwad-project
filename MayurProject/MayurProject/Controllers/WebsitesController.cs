﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MayurProject.Data;
using MayurProject.Models;

namespace MayurProject.Controllers
{
    public class WebsitesController : Controller
    {
        private readonly MayurProjectContext _context;

        public WebsitesController(MayurProjectContext context)
        {
            _context = context;
        }

        // GET: Websites
        public async Task<IActionResult> Index()
        {
            return View(await _context.Website.ToListAsync());
        }

        // GET: Websites/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website
                .FirstOrDefaultAsync(m => m.WebsiteId == id);
            if (website == null)
            {
                return NotFound();
            }

            return View(website);
        }

        // GET: Websites/Create
        public IActionResult Create()
        {
            List<Server> cl = new List<Server>();
            cl = (from c in _context.Server select c).ToList();
            cl.Insert(0, new Server { AssetId = "", ServerName = "--Select Server Name--" });
            ViewBag.message = cl;
            return View();
        }

        // POST: Websites/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("WebsiteAssetId, WebsiteId,WebsiteName,WebsiteIpAddress,WebsitePort")] Website website)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(website);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(website);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Website website)
        {
            if (ModelState.IsValid)
            {
                _context.Add(website);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View(website);
        }

        // GET: Websites/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website.FindAsync(id);
            if (website == null)
            {
                return NotFound();
            }
            return View(website);
        }

        // POST: Websites/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("WebsiteId,WebsiteName,WebsiteIpAddress,WebsitePort")] Website website)
        {
            if (id != website.WebsiteId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(website);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsiteExists(website.WebsiteId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(website);
        }

        // GET: Websites/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website
                .FirstOrDefaultAsync(m => m.WebsiteId == id);
            if (website == null)
            {
                return NotFound();
            }

            return View(website);
        }

        // POST: Websites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var website = await _context.Website.FindAsync(id);
            _context.Website.Remove(website);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WebsiteExists(string id)
        {
            return _context.Website.Any(e => e.WebsiteId == id);
        }
    }
}
