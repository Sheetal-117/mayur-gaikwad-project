﻿using MayurProject.Data;
using MayurProject.Models;
using MayurProject.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MayurProjectContext _context;

        public HomeController(ILogger<HomeController> logger, MayurProjectContext context)
        {
            _logger = logger;
            _context = context;
        }


        public IActionResult Index()
        {

            dynamic mymodel = new ExpandoObject();

            List<ServerViewModel> GetServers()
            {
                List<ServerViewModel> model = new List<ServerViewModel>();



                foreach (var ser in _context.Server)
                {

                    model.Add(new ServerViewModel
                    {

                        AssetId = ser.AssetId,
                        ServerName = ser.ServerName,
                        ServerEnvironment = ser.ServerEnvironment,
                        ServerType = ser.ServerType,
                        ServerOperatingSystem = ser.ServerOperatingSystem,
                        ServerDescription = ser.ServerDescription,
                        Websites = _context.Website.Where(w => w.AssetId == ser.AssetId).ToList(),
                        //Applications = (from a in _context.Application
                        //                join w in _context.Website
                        //                on a.WebsiteId equals w.WebsiteId
                        //                where a.WebsiteId == w.WebsiteId && a.AssetId == ser.AssetId
                        //                select a).ToList()


                    });

                }
                return model;
            }
            List<WebsiteViewModel> GetWebsite()
            {

                List<WebsiteViewModel> model1 = new List<WebsiteViewModel>();
                foreach (var ser in _context.Website)
                {

                    model1.Add(new WebsiteViewModel
                    {

                        Applications = _context.Application.Where(w => w.WebsiteId == ser.WebsiteId).ToList()


                    });

                }
                return model1;
            }





            mymodel.ServerViewModel = GetServers();
            mymodel.WebsiteViewModel = GetWebsite();






            return View(mymodel);
        }


        [HttpGet]
        public IActionResult ServerModelPartial()
        {
            Server s = new Server();
            return PartialView("ServerModelPartial", s);
        }

        [HttpPost]
        public IActionResult ServerModelPartial(Server ser)
        {
            _context.Server.Add(ser);
            _context.SaveChanges();

            return View("Index");
            //return PartialView("ServerModelPartial", ser);

        }


        [HttpGet]
        public IActionResult WebsiteModelPartial()
        {
            Website w = new Website();
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId");
            return PartialView("WebsiteModelPartial", w);
        }

        [HttpPost]
        public IActionResult WebsiteModelPartial(Website web)
        {
            _context.Website.Add(web);
            _context.SaveChanges();

            return View("Index");
            //return PartialView("ServerModelPartial", ser);

        }

        [HttpGet]
        public IActionResult ApplicationModelPartial()
        {
            Application a = new Application();
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId");
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId");
            return PartialView("ApplicationModelPartial", a);
        }

        [HttpPost]
        public IActionResult ApplicationModelPartial(Application app)
        {
            _context.Application.Add(app);
            _context.SaveChanges();

            return View("Index");
            //return PartialView("ServerModelPartial", ser);

        }

        public async Task<IActionResult> EditServerModelPartial(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var server = await _context.Server.FindAsync(id);
            if (server == null)
            {
                return NotFound();
            }

            return PartialView("EditServerModelPartial", server);
        }

        // POST: Servers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditServerModelPartial([Bind("AssetId,ServerName,ServerEnvironment,ServerType,ServerOperatingSystem,ServerDescription")] Server server)
        {


            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(server);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ServerExists(server.AssetId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(server);
        }
        private bool ServerExists(string id)
        {
            return _context.Server.Any(e => e.AssetId == id);
        }

        // GET: Servers/Delete/5
        public async Task<IActionResult> DeleteServerModelPartial(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var server = await _context.Server
                .FirstOrDefaultAsync(m => m.AssetId == id);
            if (server == null)
            {
                return NotFound();
            }

            return PartialView("DeleteServerModelPartial", server);
        }

        // POST: Servers/Delete/5
        [HttpPost, ActionName("DeleteServerModelPartial")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteServerConfirm(string id)
        {
            var server = await _context.Server.FindAsync(id);
            _context.Server.Remove(server);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Websites/Edit/5
        public async Task<IActionResult> EditWebsiteModelPartial(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website.FindAsync(id);
            if (website == null)
            {
                return NotFound();
            }
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId", website.AssetId);
            return PartialView("EditWebsiteModelPartial", website);
        }

        // POST: Websites/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditWebsiteModelPartial([Bind("AssetId, WebsiteId,WebsiteName,WebsiteIpAddress,WebsitePort")] Website website)
        {


            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(website);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WebsiteExists(website.WebsiteId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(website);
        }
        private bool WebsiteExists(string id)
        {
            return _context.Website.Any(e => e.WebsiteId == id);
        }

        // GET: Websites/Delete/5
        public async Task<IActionResult> DeleteWebsiteModelPartial(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var website = await _context.Website
                .Include(a => a.WebsiteAssetId)
                .FirstOrDefaultAsync(m => m.WebsiteId == id);
            if (website == null)
            {
                return NotFound();
            }

            return PartialView("DeleteWebsiteModelPartial", website);
        }

        // POST: Websites/Delete/5
        [HttpPost, ActionName("DeleteWebsiteModelPartial")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteWebsiteConfirm(string id)
        {
            var website = await _context.Website.FindAsync(id);
            _context.Website.Remove(website);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> EditApplicationModelPartial(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var application = await _context.Application.FindAsync(id);
            if (application == null)
            {
                return NotFound();
            }
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId", application.AssetId);
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId", application.WebsiteId);
            return PartialView("EditApplicationModelPartial", application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditApplicationModelPartial([Bind("AssetId,WebsiteId,ApplicationId,ApplicationName,ApplicationEapmId,ApplicationFtpPath")] Application application)
        {


            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(application);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationExists(application.ApplicationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssetId"] = new SelectList(_context.Server, "AssetId", "AssetId", application.AssetId);
            ViewData["WebsiteId"] = new SelectList(_context.Website, "WebsiteId", "WebsiteId", application.WebsiteId);
            return View(application);
        }

        // GET: Applications/Delete/5
        public async Task<IActionResult> DeleteApplicationModelPartial(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var application = await _context.Application
                .Include(a => a.ApplicationAssetId)
                .Include(a => a.ApplicationWebsiteId)
                .FirstOrDefaultAsync(m => m.ApplicationId == id);
            if (application == null)
            {
                return NotFound();
            }

            return PartialView("DeleteApplicationModelPartial", application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("DeleteApplicationModelPartial")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteApplicationCnfirm(string id)
        {
            var application = await _context.Application.FindAsync(id);
            _context.Application.Remove(application);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationExists(string id)
        {
            return _context.Application.Any(e => e.ApplicationId == id);
        }

        public IActionResult Create()
        {
            return View();
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("AssetId,ServerName,ServerEnvironment,ServerType,ServerOperatingSystem,ServerDescription")] Server server)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(server);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(server);
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }



}
