﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MayurProject.Models;

namespace MayurProject.Data
{
    public class MayurProjectContext : DbContext
    {
        public MayurProjectContext (DbContextOptions<MayurProjectContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<MayurProject.Models.Server> Server { get; set; }

        public DbSet<MayurProject.Models.Website> Website { get; set; }

        public DbSet<MayurProject.Models.Application> Application { get; set; }
    }
}
