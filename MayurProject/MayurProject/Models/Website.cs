﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.Models
{
    public class Website
    {
        [ForeignKey("AssetId")]
       
        public virtual Server WebsiteAssetId { get; set; }

        public string AssetId { get; set; }

        [Key]
        public string WebsiteId { get; set; }

       
        public string WebsiteName { get; set; }

        
        public string WebsiteIpAddress { get; set; }

        
        public string WebsitePort { get; set; }

        


    }
}
