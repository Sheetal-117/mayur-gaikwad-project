﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.Models
{
    public class Application
    {
        [ForeignKey("AssetId")]
        public virtual Server ApplicationAssetId { get; set; }

        public string AssetId { get; set; }

        [ForeignKey("WebsiteId")]
        public virtual Website ApplicationWebsiteId { get; set; }

        public string WebsiteId { get; set; }

        [Key]
        public string ApplicationId { get; set; }

     
        public string ApplicationName { get; set; }

       
        public string ApplicationEapmId { get; set; }

       
        public string ApplicationFtpPath { get; set; }
    }
}
