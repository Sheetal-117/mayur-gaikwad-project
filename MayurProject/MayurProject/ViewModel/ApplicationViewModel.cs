﻿using MayurProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.ViewModel
{
    public class ApplicationViewModel
    {
       
        public virtual Server ApplicationAssetId { get; set; }

        public string AssetId { get; set; }

        
        public virtual Website ApplicationWebsiteId { get; set; }

        public string WebsiteId { get; set; }

      
        public string ApplicationId { get; set; }

        [Display(Name = "Application Name")]
        public string ApplicationName { get; set; }

        [Display(Name = "EAPM Id")]
        public string ApplicationEapmId { get; set; }

        [Display(Name = "FTP Path")]
        public string ApplicationFtpPath { get; set; }


        //[Display(Name = "Application Name")]
        //public List<Application> Applications { get; set; }
    }
}
