﻿using MayurProject.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.ViewModel
{
    public class WebsiteViewModel
    {
        public string AssetId { get; set; }
        public virtual Server WebsiteAssetId { get; set; }    
        public string WebsiteId { get; set; }

        [Display(Name = "Web Site Name")]
        public string WebsiteName { get; set; }

        [Display(Name = "IP Address")]
        public string WebsiteIpAddress { get; set; }

        [Display(Name = "Service Port")]
        public string WebsitePort { get; set; }


        [Display(Name = "Application Name")]
        public List<Application> Applications { get; set; }

        

    }
}
