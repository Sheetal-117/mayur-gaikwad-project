﻿using MayurProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.ViewModel
{
    public class ServerViewModel
    {
        [Key]
       
        [Display(Name = "Asset ID")]
        public string AssetId { get; set; }



        [Display(Name = "Name")]
        public string ServerName { get; set; }

        [Display(Name = "Environment")]
        public string ServerEnvironment { get; set; }

        [Display(Name = "Type")]
        public string ServerType { get; set; }

        [Display(Name = "Operating System")]
        public string ServerOperatingSystem { get; set; }

        [Display(Name = "Server Description")]
        public string ServerDescription { get; set; }

        [Display(Name = "Application Name")]
        public List<Application> Applications { get; set; }

        [Display(Name = "Website Name")]
        public List<Website> Websites { get; set; }




    }
}
