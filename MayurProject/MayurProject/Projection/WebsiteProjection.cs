﻿using MayurProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.Projection
{
    public class WebsiteProjection
    {
       
        public virtual Server WebsiteAssetId { get; set; }
      
        public string WebsiteId { get; set; }


        public string WebsiteName { get; set; }


        public string WebsiteIpAddress { get; set; }


        public string WebsitePort { get; set; }
    }
}
