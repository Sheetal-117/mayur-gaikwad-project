﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MayurProject.Projection
{
    public class serverProjection
    {
        public string AssetId { get; set; }
        public string ServerName { get; set; }
        public string ServerEnvironment { get; set; }
        public string ServerType { get; set; }
        public string ServerOperatingSystem { get; set; }
        public string ServerDescription { get; set; }
    }
}
